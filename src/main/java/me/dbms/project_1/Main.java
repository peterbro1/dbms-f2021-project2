/**
 * DBMS project 2
 *
 * @author  Peter Browning
 * @version 1.0
 * @since   11/10/21
 */
package me.dbms.project_1;


import org.apache.commons.lang3.Validate;

import java.net.ConnectException;
import java.sql.*;
import java.util.Scanner;

public class Main {
    private static Scanner in;
    public static void main(String[] args) throws SQLException{
        if (args.length != 1) {
            System.err.format("Please start the program with arguments providing database" +
                    "details.\n Example: java -jar DBMS1.jar jdbc:mysql://localhost:3306/my_database\n");
            System.exit(0);
        }
        System.out.print("Please enter the database username:  ");
        in = new Scanner(System.in);
        String u = in.nextLine();
        System.out.print("Please enter the database password:  ");
        String p = in.nextLine();
        SQLInstance sql;
        try {
            sql = new SQLInstance(args[0], u, p);
            System.out.println("This is a mock SQL shell. Enter any SQL queries you'd like to perform, or exit() to exit.");
            while (true){
                System.out.print("SQL> ");
                String s = in.nextLine();
                if (s.equals("exit()")) System.exit(0);
                else if (s.isBlank() || s.isEmpty()) continue;

                sql.executeStatement(s);
            }
        } catch(Exception e){
            System.err.println("Something went wrong while trying to establish" +
                    "a connection to the database.");
            e.printStackTrace();
            System.exit(0);
        }


    }


}
