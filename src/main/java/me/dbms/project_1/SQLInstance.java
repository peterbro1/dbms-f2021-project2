/**
 * I was going to implement a really cool text parsing system but I did not have time,
 * so I stripped it down to this
 *
 */

package me.dbms.project_1;

import com.mysql.cj.jdbc.PreparedStatementWrapper;

import java.net.ConnectException;
import java.sql.*;
import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Wrapper for the sql Connection object. I really like taking advantage of object orientation
 * and doing stuff like this instead of having a huge and unreadable main method
 */
public class SQLInstance {
    private Connection connection;
    private String url;
    public SQLInstance(String url, String username, String password) throws ConnectException {
        this.connection = urlToConnection(url,username,password);
        this.url = url;
    }

    /**
     * Reconnects to database. Will ask for credentials again
     */
    public void reconnect(){
        String[] c = new String[2];
        Scanner s = new Scanner(System.in);
        System.out.println("Please re-enter database username.");
        c[0] = s.nextLine();
        System.out.println("Please re-enter database password.");
        c[1] = s.nextLine();
        try {
            this.connection = urlToConnection(this.url, c[0], c[1]);
        } catch(ConnectException e){
            System.err.format("There was a problem reconnecting to the database");
            System.exit(0);
        }
    }

    /**
     * Execute unsanitized SQL statement
     * @param s SQL Statement to execute
     */
    public void executeStatement(String s){
        try (Statement statement = connection.createStatement()){
            display(statement.executeQuery(s));
        }catch (Exception e){
            System.err.format("Error while executing statement: %s\n", s);
        }
        }


    /**
     * @param url JDBC Url to connect to, including database to use
     * @param username Database username
     * @param password Database password
     * @return JDBC Connection instance
     * @throws ConnectException
     */
    private Connection urlToConnection(String url, String username, String password) throws ConnectException {

        try  {
            Connection conn = DriverManager.getConnection(url, username, password);
            return conn;
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        }
        throw new ConnectException(String.format("Could not connect to %s",url));
    }

    /**
     * Display result set in a nice way. No implemented error handling!
     * @param rs
     * @throws Exception
     */
    private static void display(ResultSet rs){
        try {
            ResultSetMetaData meta = rs.getMetaData();
            int columnCount = meta.getColumnCount();
            while (rs.next()){
                for (int i = 1; i <= columnCount; i++){
                    String cont = String.format("%s: %s", meta.getColumnName(i), rs.getString(i));

                    if (i > 1) System.out.println(String.format(">  %s", cont));
                    else{
                        StringBuilder s = new StringBuilder();
                        IntStream.range(0,cont.length()+4).boxed().forEach(a -> s.append("="));
                        System.out.println("|" + s.toString() + cont + s.toString() + "|");
                    }
                }
                System.out.println();

            }
        }catch (Exception e){
            System.out.println("Error while displaying results.");
        }
    }

}
